package main

import (
	"bytes"
	"crypto/rand"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"golang.org/x/crypto/nacl/box"
)

type SecureReader struct {
	pipe      io.Reader
	pub       *[32]byte
	priv      *[32]byte
	nonce     *[24]byte
	decrypted bytes.Buffer
}

type SecureWriter struct {
	pipe      io.Writer
	pub       *[32]byte
	priv      *[32]byte
	nonce     *[24]byte
	encrypted bytes.Buffer
}

func (s SecureReader) Read(p []byte) (n int, err error) {
	// decrypt buffer

	nRead, err := s.pipe.Read(p)
	if err != nil {
		log.Fatal(err)
	}

	//log.Printf("p %v", p)
	//read the nonce first
	var nonce [24]byte

	copy(nonce[:], p[0:24])
	//log.Printf("nonce %v", nonce)
	//log.Printf("p[24:] = %v", p[24:nRead])
	dd := make([]byte, 0, nRead-24-box.Overhead)
	if dd, ok := box.Open(dd, p[24:nRead], &nonce, s.pub, s.priv); !ok {
		log.Fatal("unable to decrypt!")
	} else {
		//log.Printf("decrypted %v", dd)
		n = len(dd)
		copy(p, dd)
	}
	return
}

func (s SecureWriter) Write(p []byte) (n int, err error) {
	/*n, err = s.pipe.Write(p)
	if err != nil {
		log.Fatal(err)
	}*/
	//log.Printf("p = %v", p)
	// encrypt buffer
	nonce := *createNonce()
	sealed := make([]byte, 0, len(p)+box.Overhead)

	sealed = box.Seal(sealed, p, &nonce, s.pub, s.priv)
	//log.Printf("sealed %v", sealed)
	output := make([]byte, 0, len(sealed)+24)

	//s.encrypted = *bytes.NewBuffer(sealed)
	nn := nonce[:]
	//log.Printf("nonce %v", nn)
	output = append(nn, sealed...)
	//log.Printf("output %v", output)
	s.pipe.Write(output)

	return len(output), nil
}

func createNonce() *[24]byte {
	nonce := new([24]byte)
	_, err := io.ReadFull(rand.Reader, nonce[:])
	if err != nil {
		log.Fatal(err)
	}
	return nonce
}

// NewSecureReader instantiates a new SecureReader
func NewSecureReader(r io.Reader, priv, pub *[32]byte) io.Reader {
	sr := SecureReader{
		pipe: r,
		pub:  pub,
		priv: priv,
	}
	return sr
}

// NewSecureWriter instantiates a new SecureWriter
func NewSecureWriter(w io.Writer, priv, pub *[32]byte) io.Writer {
	sw := SecureWriter{
		pipe: w,
		pub:  pub,
		priv: priv,
	}
	return sw
}

// Dial generates a private/public key pair,
// connects to the server, perform the handshake
// and return a reader/writer.
func Dial(addr string) (io.ReadWriteCloser, error) {
	return nil, nil
}

// Serve starts a secure echo server on the given listener.
func Serve(l net.Listener) error {
	return nil
}

func main() {
	port := flag.Int("l", 0, "Listen mode. Specify port")
	flag.Parse()

	// Server mode
	if *port != 0 {
		l, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
		if err != nil {
			log.Fatal(err)
		}
		defer l.Close()
		log.Fatal(Serve(l))
	}

	// Client mode
	if len(os.Args) != 3 {
		log.Fatalf("Usage: %s <port> <message>", os.Args[0])
	}
	conn, err := Dial("localhost:" + os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	if _, err := conn.Write([]byte(os.Args[2])); err != nil {
		log.Fatal(err)
	}
	buf := make([]byte, len(os.Args[2]))
	n, err := conn.Read(buf)
	if err != nil && err != io.EOF {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", buf[:n])
}
